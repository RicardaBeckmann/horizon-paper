\documentclass{article}
\usepackage{natbib}
\usepackage{hyperref}
\usepackage{cite} 
\usepackage[usenames, dvipsnames]{xcolor}
\usepackage{graphicx}

\RequirePackage{color}\definecolor{RED}{rgb}{1,0,0}\definecolor{BLUE}{rgb}{0,0.5,1}

\title{Reply to Reviewer}
%\author{Ricarda S Beckmann}

\begin{document}
\maketitle

\section{Editor's Comments to the Author}
\textbf{
\begin{itemize}
\item Please note that the word limit for abstracts in MNRAS is 250 words. 
\item Please ensure that the keywords in your manuscript are identical to those entered on the ScholarOne submission. 
\item Please ensure that all references to arxiv papers also include the preprint number, e.g. arXiv:1506.08817. 
\end{itemize}
}

All three points have been addressed in the manuscript.

\section{Reviewer's Comments to the Author}
%Reviewer: Bower, Richard 

In response to the reviewers comments, a few substantial changes have been made to the paper, as well as a range of smaller changes to clarify various points. \textcolor{RED}{Changes in the paper have been highlighted in red.}

\begin{enumerate}
	\item We now explicitly discuss the redshift evolution of the transition mass, at which quenching due to AGN feedback becomes important, using the newly added Figure 6. (See Section 4.2, Quenching, and 6, Discussion \& Conclusions), including a comparison to observational and numerical results from literature.
	\item In Section 4.3, we now explore the origin of the evolution in accretion rate in more detail, and discuss the (lack of) impact of the shift in AGN feedback mode around redshift $z=2$, using the newly added Figure 10, updated Figure 7 and Figure 9. The conclusions have been amended accordingly.
	\item A discussion to other work on quenching by AGN feedback, both numerical and observational, has been added to the Discussion \& Conclusions in Section 6.
\end{enumerate}



\textbf{This is an interesting paper that is certainly worthy of publication in MNRAS. It describes the role that AGN feedback plays in determining the properties of galaxies in the Horizon AGN simulations. A particularly appealing aspect of the paper is the carefully twinning of galaxies in simulations with and without AGN feedback. While I have a number of significant comments on the paper, the only major omission is that there is no comparison to other work on this topic. The authors should add a section at the end comparing to the impact of AGN reported for the Illustris (Sijacki et al 2015) and EAGLE (Bower et al 2017) simulations (and perhaps other recent work), and they should address whether the processes reported in this paper are unique to the Horizon-AGN simulations or a generic feature of other simulations that include black hole feedback. The main difference between H-AGN (and Illustris) and EAGLE is that the first two simulations explicitly model two different modes of AGN feedback. The paper should clearly identify whether this is the cause of the "quenching transition mass" in the simulation. }

We have now included several paragraphs dedicated to the comparison with other work on the topic (including but not limited to the papers explicitly mentioned by the referee). These new paragraphs are mainly located in the ``discussion and conclusions'' section at the end, but also throughout the paper (see e.g. section 4.2 on Quenching). We have also added a new figure (Fig 6) to highlight this. The  issue of whether the dual feedback mode causes the ``quenching transition mass'' is now explicitly addressed in Section 4.3 (in particular we added Fig 7 and 10).  \\ 

\textbf{I provide a list of points that the revised draft should address below. I realise that several comments on S2 relate to the simulations overall, rather than to the specific topic of this paper. An answer referencing previous H-AGN papers would be sufficient. }

\textbf{S2.1 The simulations have been completed to z=0 according to the text. The authors should update the plots to show the results at z=0 (or z=0.1) in order to be comparable to observational data such as SDSS. }

$z=0.5$ has been replaced with $z=0.1$ throughout the paper. \\

\textbf{The simulation is based on an adaptive grid that expands with the expansion of the Universe. Since star formation and feedback are based on a physical (not co-moving scale) this creates conflicting requirements. My understanding of the text is that at the simulation expands, a new level of refinement is added as soon as the coarsest physical cell length exceeds 2 kpc. This must result in sudden jumps in the effect resolution of the galaxy formation physics. Since star formation occurs at a physical density threshold, this means that the mass of star forming regions jumps by a factor of 8 between z=1 and z=0. The authors should clarify this and demonstrate that this does not have a significant impact on the star formation rates of galaxies.}

There seems to be a confusion between the various criteria used to trigger refinement levels. We apologize for this and hope it is clearer from the new version of the manuscript, but here is the detailed explanation. When a new level of refinement is triggered due to the box expansion every cubic cell on the finest level of refinement that matches the quasi-Lagrangian critieria is simply split in 8 smaller cubic cells, which retain the  same gas properties (density, velocity, internal energy), due to mass, momentum \& energy conservation. 
This means that, even though the mass of gas present in each of these newly refined cells abruptly becomes 8 times smaller than that of their parent cell, their star formation rate, which only depends on the gas density, is identical. The minimum mass of a star particle is also fixed in physical units (equal to the physical density threshold for star formation times (1 kpc)$^3$ at all times, see section 2.3). This means that, provided the density of the parent cell is above the star formation threshold to begin with, one simply has to wait for 2 timesteps for the more refined cells to generate the same amount of stellar mass that their parent cell would have in a unique timestep (the courant time for the refined cells is halved since the cell size is halved). In other words, in the same amount of physical time, the same amount of stellar mass is produced whether the refinement level is triggered or not (modulo the random Poisson process). The only difference is the number of particles spawned: 8 times more particles are created in the refined case, i.e. each star particle is 8 times less massive than the single particle that would have been created had the extra refinement level not been triggered. This is again provided the mass of every particle is larger than the minimal mass allowed. In that respect, star formation rates of galaxies are completely unaffected by the instantaneous refinement, provided one puts up with particles of different masses (which is fine with PM based Poisson solvers as we discuss in our answer to S2.3 below).
  
It is true however, that such a refinement is also accompanied by a discrete change in the softening length of the gravitational force, which is reduced by a factor 2 for all cells on the finest refinement level. This means that the gas density will adjust to the stronger gravitational pull it experiences on the smallest scales. This generally leads to the gas in a fraction of the newly refined cells collapsing, and passing above the star formation threshold. The extent to which this adjustment of the gas density affects galaxy star formation rates thus depends on local gas conditions and numerical resolution and as such will vary from galaxy to galaxy. However, should it be important, its systematic nature will leave a visible imprint, ie a discrete increase, on the cosmic star formation rate history at the redshifts where extra refinement levels are triggered. This data is shown, compared to observations, on Fig 8 of \citep{Kaviraj2016}: one easily concludes from examining this figure that the effect must be fairly minor in H-AGN/H-noAGN.    

\textbf{S2.3 The comoving vs physical issue appears to mean that stars are created with masses differing by almost an order of magnitude over the course of the simulation. Could the authors demonstrate that these different particles masses do not lead to spurious gravitational effects. }

We do not fully understand what the referee means by this comment. All cosmological hydrodynamics simulations, whatever the technique used to solve the Vlasov-Poisson equation, have to deal with particles of order of magnitude mass difference: as far as we are aware, dark matter particles always are about ten times more massive than star particles (or gas particles for SPH techniques) in such simulations. So if the referee is worried about the role played by force softening in reducing spurious two body collisions between what should be collisionless particles, the answer is that we suffer from the same limitations as the well documented standard PM approach in this respect (see e.g. Hockney and Eastwood 1981) since we use a multi-grid approach to solve the Poisson equation (see \citep{Teyssier2002} for details). More specifically, if the softening length (about the size of a cell) becomes {\em much smaller} than the {\em local} mean inter-particular distance (see e.g. \citet{Knebe2000}) then our particles will undergo spurious large deflections. The quasi-Lagrangian strategy of refinement we adopt prevents this from happening, as it guarantees that the cell size scales with the total local density. Let us also point out that spawning a massive particle in the centre of the cell, or replacing it with 10 particles 10 times less massive at the same position, will lead to {\em exactly} the same force calculation using our technique: the assignation of the particle masses to the grid will lead to exactly the same contribution to the density field, hence the gravitational potential and the interpolated gravitational force on the particles will also be exactly the same. To cut a long story short, these 10 particles will remain superimposed till the end of the simulation. This is why, regardless of whether extra levels of refinement are triggered to compensate for the expansion of the box, RAMSES spawns star particles with different masses, which are always integer multiples of the fixed minimal stellar mass: it is more efficient computationally and leads to the same dynamical result as spawning N particles whose identical masses sum up to the same mass as the unique macro particle. \\


\textbf{The thermal component of the stellar feedback may simply be radiated away because of the limited resolution of the simulations. Given that the physical resolution of the simulation varies as the Universe expands, it is important that the authors demonstrate that this aspect is not resolution dependent.}

The SN feedback model employed in the H-AGN simulation (an extension of the \citet{Dubois2008} model) was thoroughly tested in various papers and at various resolutions \citep[e.g.][]{Kimm2014,Rosdahl2017}. We believe the latter paper is more relevant to the discussion at hand, as the maximum resolution of 1 kpc reached in H-AGN is insufficient to resolve the vertical scale height of virtually any disk galaxy at any redshift. This means that (i) our gas disks scale heights are of the order of 1 kpc (they are as thin as they can be, given our resolution) and (ii) our feedback model in H-AGN is very similar to the kinetic feedback model described by \citep{Rosdahl2017}, as it inevitably takes place in a bubble whose radius is larger that the scale height of our (too) thick disks. These authors conclude that such a type of 'hydrodynamically decoupled from the disk' feedback produces fast, hot outflows with mass loading factors of order unity. This will be independent of resolution since we always inject our feedback on scales larger than the scale height of our disks, whatever the redshift considered.\\

\textbf{S2.5 The simulation includes two different modes of AGN feedback. The thermal deposition mode does not appear to require that the heating rate exceeds to local cooling rate, and this energy may therefore be radiated away before significant feedback actually occurs. The authors should address the importance of the dichotomy in driving the results. For example, is there a trend for low mass galaxies to always host black holes accreting in the $\chi>1$ regime (at least when their power output is significant) while more massive galaxies host ?radio mode? black holes. What role does this effect have in establishing the galaxy transition mass? [see comment on Fig 8, below]. The authors should give more details of how the parameters were chosen for the two modes of AGN feedback. }

The referee is correct, we do not require that the heating rate exceeds the local cooling rate before injecting thermal energy. However, the thermal deposition of energy from the AGN occurs {\em instantaneously} over kpc scales given the resolution of the simulation. One could easily argue that the non-resolution of the gas density field in the vicinity of the black hole leads to an artificially low estimate of the amount of cooling, which actually takes place within this kpc scale region. Indeed, too low a local density is the argument usually invoked for the use of a 'boost factor' \citep {Booth2009} to better estimate the accretion onto SMBH. That means that the part of the thermal energy radiated away in our simulations is already underestimated. In this case, requiring that the heating rate exceeds the cooling rate would lead to an even larger underestimate. We believe these uncertainties are folded into the model parameter values, which reflect both our lack of knowledge of how AGN feedback works and our inability to numerically capture it. Note that we actually tested the impact of delaying the deposition of thermal energy until it has substantially accumulated in \citet{Dubois2012} (section 4.1.2), coming to the conclusion that it has little effect on our galaxy stellar masses at high redshift ($z>2$) when it is the dominant mode of feedback. 

The overall trend for the accretion rates onto SMBHs, and therefore the feedback mode, is mostly driven by the evolution of the gas content of galaxies (see the new Figure 10 in the paper). Irrespective of the mass of the black hole, more than 85 \% of the sample is found in quasar mode at $z>2$, and more than 80 \% in radio mode at $z<2$. Black holes in one mode or the other can be found across all black hole masses. Moreover the transition mass seems determined by the ratio $M_{SMBH}/M_{DM}$, independently of redshift (see updated Fig 11). This  supports the view that when the black hole becomes massive enough that the amount of energy it injects in the CGM is sufficient  to halt the local collapse of the halo gas, the star formation rate drops rapidly, whatever the feedback mode (with the caveat that this feedback does hydrodynamically couple to the intra-halo gas on kpc scales). Finally, our parameters for the two feedback modes are those of the best model obtained through the comparison of a host of numerical experiments presented in \citet{Dubois2012} to various datasets (sections 4.1.1, 4.1.3, 4.1.4 and 4.1.7 of the quoted paper). We thus refer to this paper for detail on how they were selected.  \\

\textbf{S3.1 The twining procedure is based on matching the spatial locations of galaxies. Since the precise location of a galaxy along its orbit will be chaotic, I would have expected that better results would have been obtained by matching orbital parameters (eg., energy and angular momentum). Could the authors explain their preference for spatial matching. }

There seems to be a confusion as to how the galaxy matching is done. Hopefully, we have clarified this in the new version of the text. The spatial matching is exclusively employed to match each galaxy to a host DM halo (or sub-halo) in the {\em same} simulation, either H-AGN or H-noAGN. To twin galaxy pairs across the two simulations, we first twin their host halos across the simulations using our particle based twinning criteria, as we can easily compute what individual DM particles these halos have in common.  Galaxies that are matched to (sub)halos twinned in this way are then declared twin galaxies. We find that this method is more reliable than  
galaxy orbital parameter matching. \\

\textbf{Does the distance requirement result in only central galaxies being twinned between haloes? Why is such a small tolerance in position required. The authors should be clear about how the centre of the halo is defined. Is this based on the most bound particle, or on the centre of mass of the system? } 

As we match both halos and subhalos across simulations, all galaxies whose DM (sub)halos survive can be matched between simulations, so both central (hosted by halos) and satellites (hosted by sub-halos) galaxies are present in our sample. Our (sub)halo based system does bias the sample against orphan galaxies, whose (sub)halos have already been destroyed. However, they represents a very small fraction of the population, and are about to merge with the central galaxy anyway. As both \citet{Peirani2016} and \citet{Chisari2017} show, relaxing the stringent distance requirement does somewhat improve the matching fraction, particularly at redshift $z=2$ and above where interactions between galaxies are more common, but also increases the number of false matches, which show up as strong outliers on mass ratio scatter plots. Since our stricter criterion does not significantly bias the results, we prefer to work with a smaller, but more robust, sample. The centre of the halo is defined with a shrinking sphere method \citep{Power2003}, and coincides with the densest particle identified in the final sphere, as indicated in \citep{Peirani2016} (section 2.2). This has been added in the text. \\

\textbf{The authors should specify a mass limit on the matching of galaxies. The text implies that only 2/3 of galaxies are matched with the 0.05Rvir criterion, but the fraction appears much higher in Table 2. The authors should clarify. What matched fraction is required to give unbiased results?}

The sample is defined in several steps:
	\begin{enumerate}
		\item Each galaxy with more than 100 stellar particles is matched with a (sub)halo, according to the 0.05Rvir criteria
		\item We apply a (sub)halo minimal mass cut of 500 DM particles
		\item We twin the (sub)halos above that minimal mass between H-AGN and H-noAGN. 
	\end{enumerate}
	
The figure quoted in the text were for step 1 in the above process, whereas the figures in Table 2 are concerned with step 3, and include only galaxies in sufficiently massive halos. We have now amended the text to make this distinction clearer. See Table \ref{tab:counts} shows a full breakdown for the number at redshift $z=5$ and $z=0.1$ for each step. 

Figure \ref{fig:sample_comparison} shows that even with the 0.05Rvir critera, and the reduced size of the sample, the results are unbiased. Indeed, comparing a sample based on an 0.05Rvir criterion with one based on an 0.1Rvir criterion at $z=3$ produces relations between galaxy and halo mass, and galaxy and black hole mass, that are indistinguishable from one another.\\

\begin{table}
	\centering
	\begin{tabular}{lcccr}
	\textbf{z} & \textbf{galaxies} & \textbf{galaxies in halos} & \textbf{galaxies in massive halos} & \textbf{twins} \\
	\hline
	4.9 & 10,222  & 8,962 (53 \%) & 5,541 (72 \%) & 4,575 (74 \%) \\
	0.1 & 113,684 & 98,034 (86 \%) & 73,880 (75 \%)  &  63,874 (86 \%) \\
	\hline
	\end{tabular}
	\caption{Number of objects at each step of sample selection for H-AGN. The number in brackets gives the percentage of galaxies from the previous column which remain after the criteria listed 
in the current column has been applied. The final sample used throughout the paper is made up of all galaxy twins, whose number is listed in the last column.}
	\label{tab:counts}
\end{table}

\begin{figure}
	\centering
	\includegraphics[width=0.8\textwidth]{sample_rvir_125}
	\caption{Sample comparison for different galaxy to (sub)halo matching criteria: $0.05\times R_{vir}$ (green) or $0.1\times R_{vir}$ (blue) at $z=3$. The top panel shows the stellar mass distribution of the final twinned sample. While the number of objects in each mass bin differs by several thousand objects, the relations are so similar as to be indistinguishable for both samples. }
	\label{fig:sample_comparison}
\end{figure}

\textbf{4.1 (Fig 3) The galaxy stellar mass function is shown in figure 3. The figure should be updated to show the mass function at z=0.1, and the simulation results compared to measurements of the mass function from the SDSS survey. The figure is unclear at present as it combines several different datasets, all with different statistical and systematic errors. Rather than showing the data as a single shaded region, it should be shown as data-points with individual error bars so that it is clear what difference are due to systematic differences between surveys vs those that are due to the limited numbers of galaxies in the survey volume. }

A comparison between the simulation data and  individual datasets with error bars, from the same set of sources as the ones used here, can be found in \citet{Kaviraj2016}, as well as a detailed analysis of the GSMF fit to observational data. In the paper presented here, the focus was intended to be on the difference between H-AGN and H-noAGN. We found that showing datasets individually detracted from the message of this plot, and so have collected them into one to broadly sketch the region occupied by observations. This has now been highlighted more clearly in the text. The mass function at z=0.1 instead of z=0.5 is now shown.\\

\textbf{The caption claims that the simulations produce "good agreement" with the observations, but this needs clarification: at stellar masses below $10^{10} M_\odot$, the simulated galaxies are a factor ~3 more abundant than in the real universe, a far greater discrepancy than can be accounted for by the statistical errors. The description of the comparison should use appropriate language. The discrepancy might suggest that supernova feedback is not efficient enough in the simulation, but the authors may wish to emphasise the magnitude limit argument in the figure caption. }

We agree with the referee about the factor ~2-3 discrepancy for low mass (below $10^{10} M_\odot$) galaxies.
We do indeed believe that this indicates that stellar feedback as implemented in H-AGN is not efficient enough to regulate star formation in these galaxies, as discussed in more detail in \citet{Kaviraj2016}. However, as the issue affects both H-AGN and H-noAGN equally (exact same model of SN feedback), we argue that any related problem cancels out when comparing the two simulations. This is important for this paper, as it allows us to draw robust conclusions about the role played by AGN feedback. The text has been rephrased to highlight this more clearly. \\

\textbf{In the text, the authors stress that "the results for galaxies with mass below 5e10 Msol have to be examined with these caveats in mind" .. however they also classify galaxies into three mass bins, all less massive than this limit and therefore subject to uncertainty. Does this mean that the results overall can be believed? The authors should explain why. }

This note was intended to remind the reader that stellar feedback is very likely not efficient enough in either H-AGN or H-noAGN for galaxies below this mass. This is relevant when examining absolute quantities such as SFR or mass outflow rates. We believe this has little impact on the results presented here, as both H-AGN and H-noAGN are equally affected by the weak stellar feedback and we rely almost entirely on relative quantities, such as mass ratios, in which case the effect should cancel out. However, in some cases we do present absolute measurements, hence the warning. We have expanded on the note in the paper to make this point more clear.\\

\textbf{S4.2 - footnote: it seems strange to define quenching in a footnote, given the fundamental importance of the definition to the paper. Many readers will find the term "quenching fraction" miss-leading since this conventional refers to the fraction of galaxies with a specific star formation rate below the main sequence. Refering to this as "quenching mass ratio" would avoid confusion. }

We agree with the referee.``Quenching mass ratio''  is now used throughout the paper, and is defined in the main body of the text. \\

\textbf{Fig 5. How does the mass range over which quenching occurs in the simulations compare to the range inferred from observations? This is discussed from an observational perspective in Peng et al 2010 for example. }

The new added Figure 6 now includes a comparison to observational data, including the transition masses presented in \citep{Peng2010}, and results from a range of simulations, and is discussed in detail in the text. \\

\textbf{The definitions of large and small galaxies used in this figure are inconsistent with those used in the introduction. Please be consistent throughout. }

We thank the referee for pointing this out. The abstract has been amended to make the mass categories consistent throughout. \\

\textbf{Fig 7 (now Fig 9). The authors note the changing importance of AGN feedback with redshift. They should demonstrate that this is driven by the evolving surface density of galaxy disks, and has nothing to do with the changing physical grid scale of the simulation. }

If the evolution in feedback mode was driven by resolution, we would expect the triggering of an extra level of refinement to \textit{increase} the accretion rate onto SMBHs, as gas would collapse further and its density would increase (see answer to S2.2). However, the global trend is for the accretion rate onto black holes to \textit{decrease} over the course of the simulation. The newly included Figure 10 now shows that the evolution of the accretion rate per unit black hole mass with redshift, $\chi / M_{SMBH}(z)$, is driven by a decreasing average gas density within the galaxies. \\

\textbf{Fig 8 (now Fig 7). The authors should make a version of this figure split into the two different modes of feedback. The thermal feedback mode may be much less effective (because of the re-radiation of the thermal energy) than the "radio mode", and the effective feedback power may not evolve as rapidly as implied by the figure. 
It would be also be very interesting to see this figure plotted as a function of black hole mass (similar to fig 9). This might make it easier to understand the simple dependence seen in Fig 9 (now Fig 11, which has changed). }

When splitting the figure into separate feedback modes, not enough objects remain per mass bin in the opposite mode, particularly in the jet mode at high redshift, for a meaningful comparison. Indeed, the vast majority of the population transitions from quasar mode to jet mode between $z=3$ and $z=1$. At z=5, only 2.6 \% of the objects are in jet mode, and at $z=0.1$, only 2.8 \% are in quasar mode. Also, as explained in answer to point S2.5, delaying thermal energy injection does not make much difference on our results at high redshift, as accretion rates are high and the amount of thermal energy deposited is large, so the effective feedback power evolution we measure is quite rapid. We feel that because the subject of this work is not so much the evolution in accretion rates of the black holes themselves, but rather the impact of feedback on the stellar mass of the galaxy, it is more appropriate to plot the feedback power against the stellar mass of the galaxy rather than the black hole mass. We refer the reader to \citep{Volonteri2016} for a detailed study of the evolving black hole population in H-AGN. However, we have changed Fig 9 (now Fig. 11) so that it is now plotted as a function of black hole to halo mass ratio instead of black hole mass. We believe this makes it easier to interpret. \\


\textbf{Fig 13 (now Fig 15). In Dubois et al 2013, it is argued that feedback from AGN suppresses star formation in the central object because in-falling cool filaments are disrupted by the presence of a hot halo. The figure seems consistent with this explanation and the authors should expand on this important point. }

It is difficult to be completely certain that cold filaments in particular are affected by AGN feedback, as large scale cosmological simulations like H-AGN lack resolution compared to zoom simulations such as the ones presented in \citet{Dubois2013}. This will impact cooling, as the cold filamentary component is under-resolved. However, we do seem to be in agreement with the conclusions in \citet{Dubois2013} that the AGN drive hot outflows reduce (possibly cold and filamentary) inflows. This agreement, as well as results from a range of other simulations, is now discussed in the main text and conclusion. \\

\subsection*{Discussion}

\textbf{The paper should include a comparison with other recent simulations that produce realistic galaxy properties. This should include (at least) the Illustris and Eagle simulations. The comparison with EAGLE is particularly interesting since there AGN feedback has essentially no impact on the formation of galaxies less massive than $10^{10.5} M_\odot$ (eg. Bower et al 2017). This leads to the existence of a transition mass above which black holes grow rapidly and most galaxies are passive. The authors should be clearer about whether a similar sharp transition occurs in Horizon AGN. I have the impression that the transition is much smoother in Horizon-AGN. }

Figure 6 now shows a quantitative measure of the transition mass, and a comparison to a range of observational and simulation data, including Bower et al (2017). In the conclusions, we have added a comparison to the impact of AGN feedback on galaxy simulation in a variety of simulation, including EAGLE, MassiveBlackII and Illustris. The smoothness of the transition mass can be evaluated from Fig 6: it is indeed smoother in H-AGN as it has a redshift dependence, contrary to EAGLE, although we have to trust the referee on this point, as the redshift evolution simulation data is not given in Bower et al (2017).  \\

\textbf{The dependence of the black hole mass versus stellar mass is also quite different in Eagle vs H-AGN. This should be included in the discussion. I believe that the EAGLE version of the relation is better supported by the available observational data (eg., Bower et 2017, Reines \& Volonteri 2016). }

The BH mass versus stellar mass relation in H-AGN is already discussed in detail in \citet{Volonteri2016}. As mentioned by these authors, the H-AGN relation agrees quite well with the low redshift data, except that the scatter is found to be substantially smaller than in observations (see Volonteri2016 Figure 5), a phenomenon also observed in EAGLE (Fig 4, Bower2017). One main difference between EAGLE and H-AGN is the shape of the $M_{BH}-M_*$ relation for low mass objects. As shown in \citep{Dubois2015,Reines2016,Bower2017}, strong stellar feedback can produce a break in the relation, and the lack of break in HORIZON could be due to the fact that our stellar feedback is comparatively weak, as discussed in Volonteri2016. However, the current limited observations for massive black holes in dwarf galaxies, and the large scatter in the existing observations, make it difficult to observationally pin down the $M_{BH}-M_*$ at low masses. We therefore prefer to refrain from a detailed discussion in this paper, and instead have added a comment in the text to refer the reader more clearly to the existing analysis in Volonteri2016

The redshift evolution of $M_{\rm SMBH}/M_*$ is a phenomenon found in a number of large scale galaxy evolution simulations, including MassiveBlackII \citep{Khandai2015} and Ilustris \citep{Sijacki2015}. It is also reported in observations by \citep{Merloni2010,Decarli2010}.  Comparing our current work with \citep{Volonteri2016} demonstrates that how apparent this redshift evolution is depends to some extent on how the slope of the relation is measured. Fitting a straight line through the sample, as done for the H-AGN simulation in \citep{Volonteri2016} returns no redshift evolution, whereas binning by stellar mass, as done in this work, exposes a change in slope for massive galaxies and black holes. We have added a brief comment to summarize this discussion in the paper, but we feel that a more detailed comparison about this relation is beyond the scope of the paper as it was already performed in \citep{Volonteri2016}.\\

\textbf{Unlike EAGLE (in which there is a single mode of AGN feedback), Horizon and Illustris include two different modes of feedback, with a transition in the effective efficiency and coupling change as a function of the relative accretion rate. The authors should discuss whether this is what drives the transition mass (and hence the change in galaxy mass function slope) seen in the simulations. }

As now discussed in the paper, the evolution in transition mass is primarily driven by an evolution in the $M_{SMBH}-M_*$ relation (and accretion rate). The transition in feedback mode would actually induce the opposite trend to the one observed here, as a black hole would subject its host galaxy to stronger feedback just after transitioning to jet mode, due to its higher radiative efficiency. Instead, we find that AGN power is a smoothly decreasing function of redshift (see new Fig 7). The evolution in quenching mass ratio in our simulations, and the underlying evolution in AGN power, is mainly driven by a redshift dependent $M_{SMBH}-M_*$ relation. The change in average accretion rate due to decreasing average density of gas in the galaxy would happen regardless of the shift in feedback mode at low redshift. Indeed, if we were to re-run the H-AGN simulation with only the quasar mode, we expect the simulation would be nearly identical up to be point where the radio mode objects represent a significant fraction of the sample, i.e. $z\sim1.5$, by which point it is obvious from Fig 10 that the average gas density of galaxies has already dropped significantly. This change in accretion rate reduces the amount of AGN feedback in a way that the switch of feedback mode is unable to compensate for. \\

\subsection*{Conclusions} 
\textbf{The conclusions should be updated in line with the other changes in the paper. The authors should address whether the effectiveness of AGN feedback and the existence of a transition mass is the result of a transition from "quasar mode" to "radio mode" as the average gas density declines with cosmic time, or whether it is driven by other physics in the simulation. }

This issue has been addressed, both in the main body of the text (Section 4.2, Figure 6), as well as in the conclusions. As previously mentioned, we find that the transition in feedback mode has little influence on the transition mass. We do observe a decreasing average gas density in galaxies over cosmic time, but this effect is independent of the feedback mode, since it is a result of the competition between depletion by star formation vs replenishment by accretion of halo gas. Indeed, such a decrease also happens in the H-noAGN simulation. \\

\subsection*{Minor suggestions that can be easily addressed}

\textbf{The abstract (and generally throughout the paper). The paper should precisely define what is meant by (e.g.) "the massive end of the mass function?, and clearly state what is meant by giving a mass. }

This has been done systematically. \\

\textbf{The wording of the last sentences of the abstract is unclear: Are smaller BH harboured by galaxies at higher redshift or lower redshift? Is AGN feedback stronger or weaker at higher redshift? }

This has been rewritten. \\

\textbf{Introduction: The effectiveness of feedback is very dependent on the implementation scheme used. Is it safe to conclude that starburst feedback can never quench massive galaxies regardless of implementation? }

For any reasonable IMF, it is. There is simply not enough energy released by massive stars to unbind/heat up a significant fraction of the gas over a significant amount of cosmic time in these galaxies.  This argument has been known since the seminal work of \citep{Dekel1986} and, to the best of our knowledge, no implementation of stellar feedback to date has managed to overturn this conclusion, including the work by the FIRE group which uses some of the most extreme and complete stellar feedback models \citep{Hopkins2014,Hopkins2017}. The best they manage is to reduce the stellar mass in Milky Way type galaxies by a factor 2-3 compared to the total baryon mass present in the host dark matter halo (see figure 7 of \citet{Hopkins2017}). This will only get worse for more massive galaxies embedded in more massive halos, in particular for galaxies at the centre of clusters which are observed to have undergone little star formation in the past several billion years: how could such galaxies be quenched, and remain quenched, by stellar feedback alone?   \\

\textbf{``outflows are common in maintenance mode radio quasars''. The authors should avoid mixing observational evidence and its theoretical interpretation. }
This has been rephrased.

\textbf{S2.4 The authors should clarify that eq (1) does not account for the angular momentum of gas surrounding the blackhole, and explain the motivation for the choice of eq (2) (why a power 2 rather than some other factor?) }

There are a lot of things eq (1) does not account for. Angular momentum of the gas on the Bondi radius scale is one of them (or density and velocity gradients), but hydrodynamical effects (gas pressure) are also neglected in the case of supersonic motion of the BH relative to the gas, and so are hydrodynamical instabilities (e.g. advective-acoustic instability) which develop on sub-Bondi radius scale in that case. We have added a sentence to state this. We use a power of 2 as \citet{Booth2009} and \citet{Dubois2012} came to the conclusion that this seems to be the best parametric choice to match observations: we refer to these papers for detail.\\

\textbf{S4.3 The authors suggest that the BH accretion rate depends on the mass squared - but this is only the case if the black holes are in the Bondi limited regime: in the Eddington regime the accretion rate is linear with mass.}

This has now been explicitly stated. \\

\bibliographystyle{humannat}
\bibliography{references} 

\end{document}
