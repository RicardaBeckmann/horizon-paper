#!/bin/bash

#python python_plots$pic_pathy

pic_path=$(pwd)

cd Hquenching/

cp $pic_path/matched_numbers_mass_AGN_4_nbins20_r05.pdf ./Fig2.pdf
cp $pic_path/GMF_four_20.pdf ./Fig3.pdf
cp $pic_path/ratios_AGN.halo.host.mvir_AGN.halo.host.mvir_quartile_4_nbins20.pdf ./Fig4.pdf
cp $pic_path/ratios_AGN.halo.mvir_AGN.halo.mvir_quartile_4_nbins20.pdf ./Fig5.pdf
cp $pic_path/transition_mass.pdf ./Fig6.pdf
cp $pic_path/binned_AGN.halo.mvir_AGN.mark.sink.AGN_power_quartile_4_nbins20.pdf ./Fig7.pdf
cp $pic_path/binned_AGN.halo.mvir_AGN.mark.sink.mass_quartile_4_nbins20.pdf  ./Fig8.pdf
cp $pic_path/scatter_4_eddington_ratio_mass_4.pdf ./Fig9.pdf
cp $pic_path/binned_AGN.mark.GALrho_gas_AGN.mark.sink.eddington_mbh_quartile_4_nbins20.pdf ./Fig10.pdf
cp $pic_path/ratios_AGN.mark.MbhMdm_AGN.halo.mvir_quartile_4_nbins20.pdf ./Fig11.pdf

#cp $pic_path/ratios_AGN.mark.sink.mass_AGN.halo.mvir_quartile_4_nbins20.pdf ./Fig9.pdf
cp $pic_path/ratios_AGN.halo.mvir_AGN.mark.Mbaryon_vol_quartile_4_nbins20.pdf ./Fig12.pdf
cp $pic_path/flowratios_masses_quartile_R95_4.pdf ./Fig15a.pdf
cp $pic_path/flowratios_masses_quartile_R20_4.pdf ./Fig15b.pdf
cp $pic_path/flows_masses_R95_4.pdf ./Fig13a.pdf
cp $pic_path/flows_masses_R20_4.pdf ./Fig13b.pdf
cp $pic_path/residual_flows_masses_R20_4.pdf ./Fig14b.pdf
cp $pic_path/residual_flows_masses_R95_4.pdf ./Fig14a.pdf
cp $pic_path/dutycycle_flows_10_redshift.pdf ./Fig16.pdf


#Reviewer reply only
cp $pic_path/sample_*.pdf ./ReviewerReply/
