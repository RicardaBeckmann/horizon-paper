import horizon
import timeseries_horizon

xlabel=r"Stellar mass: $ M_{*}^{\rm H-AGN} [{\rm M_{\odot}}$]"


#GMF including Bernardi data                                                                                                                                        
horizon.four_GMF()

#Galaxy mass ratio plot                                                                                                                                              
horizon.binned_field_mass(quartile=True,ylabel=r"Stellar mass ratios: $ M_{*}^{\rm H-AGN} / M_{*}^{\rm H-noAGN}$",xlabel=xlabel,ratios=True)

horizon.transition_mass()

#DM halo mass ratio plots                                                                                                                                            
horizon.binned_field_mass(quartile=True,ylabel=r"Halo mass ratios: $M_{\rm vir}^{\rm H-AGN} / M_{\rm vir}^{\rm H-noAGN}$",xlabel=r"Halo mass: $ M_{\rm vir}^{\rm H-AGN} [{\rm M_{\odot}}$]",x='AGN.halo.host.mvir',ratios=True,y='AGN.halo.host.mvir')

#Ratios_BHmass plot                                                                                                                                                  
horizon.binned_field_mass(quartile=True,ylabel=r"Quenching mass ratio: $ M_{*}^{\rm H-AGN} / M_{*}^{\rm H-noAGN}$",xlabel=r"$ M_{\rm SMBH}/M^{\rm H-AGN}_{\rm vir}$",x='AGN.mark.MbhMdm',ratios=True)

horizon.binned_field_mass(x='AGN.mark.sink.mass',ylabel=r"Stellar mass ratios: $M_{*}^{\rm H-AGN} / M_{*}^{\rm H-noAGN}$",quartile=True,xlabel=r"Central black hole mass: $\rm M_{\rm BH}^{\rm H-AGN} [{\rm M_{\odot}}$]",ratios=True)

#BHmass_galaxy mass plot
horizon.binned_field_mass(quartile=True,xlabel=xlabel,y='AGN.mark.sink.mass',ylabel=r'Central black hole mass $M_{\rm SMBH}$ [M$_\odot$]')

#Baryon mass ratio plot
horizon.binned_field_mass(quartile=True,ylabel=r" Baryon density ratios: $ M_{\rm bar}^{\rm H-AGN} / M_{\rm bar}^{\rm H-noAGN}$",xlabel=xlabel,y='AGN.mark.Mbaryon_vol',ratios=True)

#flow plots
horizon.flowratios_mass(xlabel=xlabel)

#duty cycle plot
timeseries_horizon.duty_cycle(time=False)

#Eddington ratio scatter
horizon.four_scatter(y='AGN.mark.sink.eddington_ratio',xlabel=r"Central black hole mass: $ M_{\rm BH}^{\rm H-AGN} [{\rm M_{\odot}}$]",ylabel=r'Eddington ratio $\chi= \dot{M}_{\rm Bondi}/\dot{M}_{\rm Edd}$',x='AGN.mark.sink.mass')

#Evolution of eddington ratio
horizon.binned_field_mass(y='AGN.mark.sink.eddington_mbh',ylabel=r"$\chi / M_{\rm SMBH}= \dot{M}_{\rm Bondi}/(M_{SMBH}\times \dot{M}_{\rm Edd})$",xlabel=r"average galactic gas density $\rho^{\rm H-AGN}_{\rm gas}$ [M$_\odot$/kpc$^3$]",x='AGN.mark.GALrho_gas')

#AGN power plot
horizon.binned_field_mass(y='AGN.mark.sink.AGN_power',ylabel=r'AGN power [$\rm M_\odot c^2 / yr$]',xlabel=xlabel)


###########################

#Matching numbers (very slow,hence last)                                                                                                                             
horizon.matched_numbers_mass(xlabel=r"Object mass $M^{\rm H-AGN}$ [M$_\odot$]",ylabel=r'Matching fraction: $ N_{\rm twinned} / N^{\rm H-AGN}$')
